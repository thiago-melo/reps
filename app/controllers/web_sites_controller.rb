class WebSitesController < ApplicationController
  def index
    #@universities = University.where(active: true)
    #@republics = Republic.where(active: true).where(is_people: true)
  end
  
  def change_lang
    setSystemLanguage(params[:lang])
    redirect_to root_path
  end
  
  def mobile
    render :layout => false
  end
  
  def thiago_melo_resume_pt
    render :layout => false
  end
  
  def thiago_melo_resume_en
    render :layout => false
  end
  
  def search
    @university = University.find_by(acronym: params[:university])
    setSystemUniversity(@university.acronym)
    
    @types = rep_types
    @genders = rep_genders
    @shape = -1
    @max_cost = 3200
    @gender = -1
  end
  
  def search_filter
    @types = rep_types
    @genders = rep_genders
    @shape = params[:shape]
    @max_cost = params[:max_cost]
    @gender = params[:gender]
    render :search
  end
  
  def search_people
    @university = University.find_by(acronym: params[:university])
    setSystemUniversity(@university.acronym)
    @republics = Republic.where(active: true).where(is_people: true).where(university_id: @university.id)
  end
  
  def register_vacancy
    @user = User.new
    @republic = Republic.new
    @types = rep_types
    @genders = rep_genders
    @type = "vacancy"
    render :register #"register.html.erb"
  end
  
  def register
    @user = User.new
    @republic = Republic.new
    @types = rep_types
    @genders = rep_genders
  end
  
  def register_property
    @user = User.new
    @republic = Republic.new
    @types = rep_types
    @genders = rep_genders
    @type = "property"
    render :register
  end
  
  def register_people
    @user = User.new
    @republic = Republic.new
    @types = rep_types
    @genders = rep_genders
    @type = "people"
    render :register
  end
  
  def maps
    @republics = Republic.where(latitude: nil)
  end
  
  def enable
    @republics = Republic.all
  end
  
  def email_user
    @rep = Republic.find(params[:rep_id])
  end
  
  def partners_help
    @partners = Partner.all
  end
  
  def partners
    @partners = Partner.all
  end
  
  def listing_preview
    if !params[:id].nil?
      @republic = Republic.find(params[:id])
    else
      redirect_to root_path
    end
  end
  
  def university_help
    @universities = University.order(acronym: :asc).all
  end
  
  def questions_help
    @universities = University.order(acronym: :asc).all
  end
  
  # ajax methods
  
  def new_partner_ajax
    partner = Partner.new(partner_params)
    if partner.save
      if params[:partner_logo]
        partner.logo = save_image_to_s3(params[:partner_logo], "partners", "logo_#{partner.id}")
        partner.save
      end
      if params[:partner_small_logo]
        partner.small_logo = save_image_to_s3(params[:partner_small_logo], "partners", "small_logo_#{partner.id}")
        partner.save
      end
      if params[:partner_picture]
        image = params[:partner_picture]
        uploaded_io = image
        filename = uploaded_io.original_filename
        extension = filename.split('.').last.downcase
        tmp_file = "#{Rails.root}/public/partners/picture_#{partner.id}.#{extension}"
        partner.picture = "partners/picture_#{partner.id}.#{extension}"
        File.open(tmp_file, 'wb') do |f|
          f.write uploaded_io.read
        end
        partner.save
      end
      container = { "partners" => Partner.all, "status" => "success"}
    else
      container = { "status" => "fail"}
    end
    render :json => container.to_json
  end
  
  def partner_info_ajax
    partner = Partner.find(params[:partner_id])
    if !partner.nil?
      container = { "partner" => partner, "status" => "success"}
    else
      container = { "status" => "fail"}
    end
    render :json => container.to_json
  end
  
  def university_info_ajax
    university = University.find(params[:university_id])
    if !university.nil?
      container = { "university" => university, "status" => "success"}
    else
      container = { "status" => "fail"}
    end
    render :json => container.to_json
  end
  
  def update_partner_ajax
    partner = Partner.find(params[:partner_id])
    if partner.update_attributes(partner_params)
      container = { "partner" => partner, "status" => "success"}
    else
      container = { "status" => "fail"}
    end
    render :json => container.to_json
  end
  
  def update_university_ajax
    university = University.find(params[:university_id])
    if university.update_attributes(university_params)
      container = { "university" => university, "status" => "success"}
    else
      container = { "status" => "fail"}
    end
    render :json => container.to_json
  end
  
  def new_university_ajax
    university = University.new(university_params)
    if university.save
      if params[:university_logo]
        university.logo = save_image_to_s3(params[:university_logo], "universities", "logo_#{university.id}")
        university.save
      end
      if params[:university_small_logo]
        university.small_logo = save_image_to_s3(params[:university_small_logo], "universities", "small_logo_#{university.id}")
        university.save
      end
      container = { "universities" => University.all, "status" => "success"}
    else
      container = { "status" => "fail"}
    end
    render :json => container.to_json
  end
  
  def new_message
    message = Message.new(message_params)
    message.active = true
    if message.save
      saveUserPersonalityInfo(params[:message][:name], params[:message][:email])
      container = { "status" => "success"}
    else
      container = { "status" => "fail"}
    end
    render :json => container.to_json
  end
  
  def save_maps_ajax
    republic = Republic.find(params[:id])
    republic.latitude = params[:latitude]
    republic.longitude = params[:longitude]
    if republic.save
      container = { "status" => "success"}
    else
      container = { "status" => "fail"}
    end
    render :json => container.to_json
  end
  
  def activate_ajax
    republic = Republic.find(params[:id])
    republic.active = params[:status] == "1" ? true : false
    if republic.save
      container = { "status" => "success"}
    else
      container = { "status" => "fail"}
    end
    render :json => container.to_json
  end
  
  def enable_delete_ajax
    republic = Republic.find(params[:id])
    pictures = republic.pictures.all
    
    pictures.each do |pic|
      begin
        File.delete("#{Rails.root}/public/#{pic.path}")
      rescue
      
      ensure
        pic.destroy
      end
    end
    container = { "status" => "success"}
    render :json => container.to_json
  end
  
  def update_coordinates_ajax
    republic = Republic.find(params[:id])
    republic.latitude = params[:latitude]
    republic.longitude = params[:longitude]
    if republic.save
      container = { "status" => "success"}
    else
      container = { "status" => "fail"}
    end
    render :json => container.to_json
  end
  
  # this method sets the systemUniversity cookie to be
  # the university received as param
  #
  # @params: university
  #
  # @author: Thiago Melo
  # @version: 2015-03-14
  def set_system_university_ajax
    setSystemUniversity(params[:university])
    container = { "status" => "success"}
    render :json => container.to_json
  end
  
  # this method should return the questions belonging to a university
  #
  # the response is given through a JSON format
  #
  # @params: [university_id]
  #
  # @author: Thiago Melo
  # @version: 2015-03-19
  def university_questions_ajax
    if params[:university_id].present?
      questions = Question.where(university_id: params[:university_id], language: params[:language])
      answers = Hash.new
      questions.each_with_index do |question, index|
        answers[index] = question.answers
      end
      container = { "status" => "success", "questions" => questions, "answers" => answers }
    else
      container = { "status" => "missing_param"}
    end
    render :json => container.to_json
  end
  
  # this method should save a new question with its corresponding
  # answers
  #
  # @params: [question][question] the question
  #          [question][max_answers] the max answers allowed for this question
  #          [question][university_id] the university its question belongs
  #          
  #          [answers_number] the number of answers
  #          [answer_i] the answers, i in [0..n]
  #
  # @author: Thiago Melo
  # @version: 2015-03-20
  def new_question_ajax
    question = Question.new(question_params)
    question.active = true
    if question.save
      answers_number = params[:answers_number].to_i
      for i in 0..answers_number
        begin
          answer = Answer.new
          answer.question_id = question.id
          answer.answer = params["answer_#{i}"]
          answer.active = true
          answer.save!
        rescue
          # do nothing
        end
        container = { "status" => "success"}
      end
    else
      container = { "status" => "fail"}
    end
    render :json => container.to_json
  end
  
  # this method return the answers for a question
  #
  # @params: [question_id]
  #
  # @author: Thiago Melo
  # @version: 2015-03-19
  def question_answers_ajax
    if params[:question_id].present?
      question = Question.find(params[:question_id])
      answers = question.answers
      container = { "status" => "success", "answers" => answers, "question" => question }
    else
      container = { "status" => "fail"}
    end
    render :json => container.to_json
  end
  
  # this method updates a question attributes question, max_answers and active
  # it still accepts new answers
  #
  # @params: [question_id] the id of the question to be updated
  #           [question][question] the question text
  #           [question][max_answers] the question max answers allowed number
  #           [question][active] if the question is active
  #
  #           [new_answers_number] the number of answers
  #           [new_answer_i] the answers, i in [0..n]
  #
  # @author: Thiago Melo
  # @version: 2015-03-25
  def update_question_ajax
    question = Question.find(params[:question_id])
    if !question.nil?
      old_answers = question.answers
      if question.update_attributes(question_update_params)
        new_answers_number = params[:new_answers_number].to_i
        for i in 0..(new_answers_number - 1)
          answer = Answer.new
          answer.question_id = question.id
          answer.answer = params["new_answer_#{i}"]
          answer.active = true
          answer.save!
        end
        container = { "status" => "success", "question" => question, "old_answers" => old_answers }
      else
        container = { "status" => "fail" }
      end
    else
      container = { "status" => "fail" }
    end
    render :json => container.to_json
  end
  
  # this method updates a answer's attributes 
  #
  # @params: [answer_id] the id of the answer to be updated
  #           [answer][answer] the answer text
  #           [answer][active] if the answer is active
  #
  #           [new_answers_number] the number of answers
  #           [new_answer_i] the answers, i in [0..n]
  #
  # @author: Thiago Melo
  # @version: 2015-03-25
  def update_answer_ajax
    answer = Answer.find(params[:answer_id])
    if !answer.nil?
      if answer.update_attributes(answer_update_params)
        container = { "status" => "success" }
      else
        container = { "status" => "fail" }
      end
    else
      container = { "status" => "fail" }
    end
    render :json => container.to_json
  end
  
  # this method should take care of all compatibility questions and go over
  # all possible condictions to store and continue the asking process
  #
  # @params:
  #
  #
  # @author: Thiago Melo
  # @version: 2015-03-27
  #
  def answer_compatibility_process_ajax
    
    questions = Question.where("language = ? and university_id = ? and active = ?", systemLanguage, systemUniversityID, true).order("id asc")
    
    current_question = nil
    current_answers = nil
    title = nil
    
    if logged_in?
      user = current_user
      questions.each_with_index do |question, index|
        user_answer = UserAnswer.where("user_id = ? and question_id = ?", user.id, question.id)
        if user_answer.length == 0
          current_question = question
          title = "#{getTextMessage("question")} #{(index + 1).to_s}/#{questions.length}"
        end
        break if user_answer.length == 0
      end
    else
      if !cookies[:compatibility_answers].present? and questions.length > 0
        current_question = questions[0]
        title = "#{getTextMessage("question")} #{(1).to_s}/#{questions.length}"
      else
        cookie_answers = ActiveSupport::JSON.decode(cookies[:compatibility_answers])
        questions.each_with_index do |question, index|
          if cookie_answers[question.id].nil? 
            current_question = question
            title = "#{getTextMessage("question")} #{(index + 1).to_s}/#{questions.length}"
          end
          break if cookie_answers[question.id].nil?
        end
      end
    end
    
    if !current_question.nil?
      current_answers = current_question.answers
      container = { "status" => "success", "question" => current_question, "answers" => current_answers, "title" => title }
    else
      container = { "status" => "done" }
    end
    render :json => container.to_json
  end
  
  # this method saves the answer for a question
  # if there is a user logged, it stores the answer on database
  # otherwise, it stores the answer in the cash
  #
  # @params: [question_id] the corresponding question ID
  #          [answers_ids] it is a string with the answers id as follow:
  #          => 1, 3 and 5 => 1|3|5
  #
  # @author: Thiago Melo
  # @version: 2015-03-27
  #
  def quick_answer_ajax
    begin
      if params[:question_id].present? and params[:answers_ids].present?
        # to make it fast
        question = Question.find(params[:question_id])
        question_id = question.id
        
        # check this later
        answers_ids = params[:answers_ids].split("|")
        if answers_ids.length > 3
          answers_ids.slice!(0, 3)
        end
        
        if !question.nil?
          if logged_in?
            # to make it fast
            user = current_user
            user_id = user.id
            
            # check if user already has this answer saved
            current_answers = UserAnswer.where("user_id = ? and question_id = ?", user_id, question_id)
            if current_answers.length > 0
              current_answers.each do |answer|
                answers_ids.each do |new_answer|
                  if answer.id == new_answer
                    answers_ids -= [new_answer]
                  end
                end
              end
            end
            
            answers_ids.each do |new_answer|
              user_answer = UserAnswer.new do |ua|
                ua.user_id = user_id
                ua.answer_id = new_answer
                ua.question_id = question_id
              end
              user_answer.save
            end
            container = { "status" => "success" }
          else
            if cookies[:compatibility_answers].present?
              # please review it when you could
              cookie_answers = ActiveSupport::JSON.decode(cookies[:compatibility_answers])
              if !cookie_answers.include?(question_id)
                cookie_answers[question_id] = Array.new
              end
              
              answers_ids.each do |new_answer|
                if !cookie_answers[question_id].include?(new_answer)
                  cookie_answers[question_id] += [new_answer]
                end
              end
            else
              cookie_answers = Array.new
              cookie_answers[question_id] = Array.new
              answers_ids.each do |new_answer|
                cookie_answers[question_id] += [new_answer]
              end
            end
            # please review it when you could
            cookies[:compatibility_answers] = ActiveSupport::JSON.encode(cookie_answers)
            container = { "status" => "success" }
          end
        else
          container = { "status" => "question_not_found" }
        end
      else 
        container = { "status" => "empty" }
      end
    rescue
      container = { "status" => "fail" }
    end
    render :json => container.to_json
  end
  
  # this method resets all compatibility answers saved on chache
  # or for a logged user
  #
  # @author: Thiago Melo
  # @version: 2015-03-27
  #
  def reset_questions_ajax
    begin
      if logged_in?
        UserAnswer.where("user_id = ?", current_user.id).destroy_all
      else
        cookies[:compatibility_answers] = nil
      end
      container = { "status" => "success" }
    rescue
      container = { "status" => "fail" }
    end
    render :json => container.to_json
  end
  
  private
    def message_params
      params.require(:message).permit(:name, :email, :subject, :body)
    end
    
    def partner_params
      params.require(:partner).permit(:name, :email, :address, :email, :website, :facebook, :phone, 
        :latitude, :longitude, :active, :description)
    end
  
    def university_params
      params.require(:university).permit(:name, :acronym, :latitude, :longitude, :zoom, 
        :active)
    end
    
    def question_params
      params.require(:question).permit(:question, :university_id, :max_answers, :language)
    end
    
    def question_update_params
      params.require(:question).permit(:question, :max_answers, :active)
    end
    
    def answer_update_params
      params.require(:answer).permit(:answer, :active)
    end
end
