class RepublicsController < ApplicationController
  def list
  end
  
  def phone_views_number
    @republics = Republic.all.order("phone_views DESC")
  end
  
  # ajax methods
  def repuplic_information_ajax
    if !logged_in?
      redirect_to root_path
    end
    republic = Republic.find(params[:republic_id])
    if !republic.nil?
      container = { "republic" => republic, "pictures" => republic.pictures.all, "rep_types" => rep_types, "rep_genders" => rep_genders, "status" => "success",
                "lease_options" => lease_options, "organization_options" => organization_options,
                "parties_options" => parties_options, "stuff_options" => stuff_options }
    else
      container = { "republic" => nil, "status" => "fail"}
    end
    render :json => container.to_json
  end
  
  def delete_image_ajax
    if !logged_in?
      redirect_to root_path
    end
    picture = Picture.find(params[:picture_id])
    delete_image_from_s3(picture.path.clone)
    if picture.destroy
      container = { "status" => "success"}
    else
      container = { "status" => "fail"}
    end
    render :json => container.to_json
  end
  
  def upload_image_ajax
    if !logged_in?
      redirect_to root_path
    end
    picture = Picture.new
    picture.main = 0
    picture.path = "temp"
    picture.republic_id = params[:republic_id]
    
    if picture.save
      picture.path = save_image_to_s3(params[:image], "pictures", "#{picture.republic_id}_#{picture.id}")
      if picture.save
        container = { "picture_id" => picture.id, "status" => "success"}
      else
        container = { "status" => "fail", "error" => picture.errors.full_messages}
      end
    else
      container = { "status" => "fail", "error" => picture.errors.full_messages}
    end
    render :json => container.to_json
  end
  
  def update_republic_ajax
    if !logged_in?
      redirect_to root_path
    end
    
    republic = Republic.find(params[:republic][:id])
    
    if republic.user_id != current_user.id
      redirect_to root_path
    end
    
    if republic.update_attributes(republic_params)
	    container = { "status" => "success"}
	  else
      container = { "status" => "fail"}
    end
    render :json => container.to_json
  end
  
  
  def filter_republics_ajax
    per_page = 16
    
    republics = Republic.where("university_id = #{params[:university_id]}")
    
    #is_landlord_alias = params[:is_landlord] == "true" ? true : false
    republics = republics.where("cost <= #{params[:cost_max]}")
    republics = republics.where("cost >= #{params[:cost_min]}")
    
    #republics = republics.where(is_landlord: is_landlord_alias)
    republics = republics.where(is_people: false)
    republics = republics.where(active: true)
    
    if params[:bathrooms] != "-1"
      republics = republics.where("bathrooms >= ?", params[:bathrooms].to_i)
    end
    if params[:pets] != "-1"
      republics = republics.where(pets: params[:pets] == "true" ? true : false)
    end
    if params[:parking] != "-1"
      republics = republics.where(parking: params[:parking] == "true" ? true : false)
    end
    if params[:heating] != "-1"
      republics = republics.where(heating: params[:heating] == "true" ? true : false)
    end
    if params[:smoking] != "-1"
      republics = republics.where(smoking: params[:smoking] == "true" ? true : false)
    end
    if params[:alcohol] != "-1"
      republics = republics.where(alcohol: params[:alcohol] == "true" ? true : false)
    end
    if params[:organization] != "-1"
      republics = republics.where(organization: params[:organization])
    end
    if params[:parties] != "-1"
      republics = republics.where(parties: params[:parties])
    end
    if params[:stuff] != "-1"
      republics = republics.where(stuff: params[:stuff])
    end
    if params[:lease] != "-1"
      republics = republics.where(lease: params[:lease])
    end
    if params[:gender] != "-1"
      republics = republics.where("gender = #{params[:gender]}")
    end
    if params[:shape] != "-1"
      republics = republics.where("shape = #{params[:shape]}")
    end
    if params[:roommates].to_f > 0
      republics = republics.where("roommates = #{params[:roommates]}")
    end
    
    pictures_paths = {}
    users_info = {}
    scores = {}
    
    total_pages_under_filter = (republics.length.to_f / per_page.to_f).ceil
    
    # are you stupid???
    #republics = republics.order("RANDOM()").page(params[:page]).per(16) #order(:cost)
    
    #republics = republics.order(pictures_count: :desc)
    
    if cookies[:compatibility_answers].present?
      cookie_answers = ActiveSupport::JSON.decode(cookies[:compatibility_answers])
      #cookie_answers.each_with_index do |cookie, index|
      #  if !cookie.present?
      #    cookie_answers.delete_at(index)
      #  end
      #end
      
      republics.each do |r|
        scores[r.id] = r.userCompatibility(cookie_answers, systemUniversityID, systemLanguage)
      end
      
      for i in 0..(republics.length - 2)
        for j in i..(republics.length - 1)
          if scores[republics[i].id] < scores[republics[j].id] or (scores[republics[i].id] == scores[republics[j].id] and republics[i].pictures_count < republics[j].pictures_count)
            aux = republics[i]
            republics[i] = republics[j]
            republics[j] = aux
          end
        end
      end
      republics = republics.slice((params[:page].to_i - 1) * per_page, per_page )
    else
      republics = republics.order(pictures_count: :desc)
      republics = republics.paginate(:per_page => per_page, :page => params[:page])
    end
    
    republics.each do |r|
      if r.pictures.length > 0
        pictures_paths[r.id] = r.pictures
      else
        pictures_paths[r.id] = nil
      end
      users_info[r.id] = r.user.clean_for_ajax
    end
    
    #if cookies[:compatibility_answers].present?
    #  cookie_answers = ActiveSupport::JSON.decode(cookies[:compatibility_answers])
    #  republics.each do |r|
    #    scores[r.id] = r.userCompatibility(cookie_answers)
    #  end
    #end
    
    
    #with_pictures = Array.new
    #without_pictures = Array.new
    
    #republics.each do |r|
    #  if r.pictures.length > 0
    #    pictures_paths[r.id] = r.pictures
    #    with_pictures.push(r)
    #  else
    #    pictures_paths[r.id] = nil
    #    without_pictures.push(r)
    #  end
    #  users_info[r.id] = r.user.clean_for_ajax
    #end
    
    #republics = with_pictures.push(*without_pictures)
    
    container = { "status" => "success", "republics" => republics, "pictures_paths" => pictures_paths, 
                "rep_types" => rep_types, "rep_genders" => rep_genders, "users_info" => users_info,
                "lease_options" => lease_options, "organization_options" => organization_options,
                "parties_options" => parties_options, "stuff_options" => stuff_options,
                "partners" => Partner.where(active: true), "total_pages_under_filter" => total_pages_under_filter,
                "scores" => scores }
    render :json => container.to_json
  end
  
  def increment_phone_views_ajax
    republic = Republic.find(params[:republic_id])
    if !republic.nil?
      phone_views = republic.phone_views
      republic.update_attribute(:phone_views, phone_views + 1)
      container = { "status" => "success"}
	  else
      container = { "status" => "fail"}
    end
    render :json => container.to_json
  end
  
  private
    def republic_params
      params.require(:republic).permit(:title, :is_people, :is_landlord, :address, :number, :shape, 
        :hide_address, :gender, :cost, :roommates, :description, :pets, :smoking, :alcohol,
        :stuff, :bathrooms, :heating, :parking, :parties, :organization, :lease, :university_id)
    end
    
end