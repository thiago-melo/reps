class SessionsController < ApplicationController
  def new
    
  end
  
  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      log_in user
      params[:session][:remember_me] == '1' ? remember(user) : forget(user)
      redirect_to user_home_path
    else
      flash[:danger] = getTextMessage("login_fail_message")
      redirect_to root_url
    end
  end
  
  def destroy
    log_out if logged_in?
    redirect_to root_url
  end
  
  #----------- alternative logins --------
  
  # this should perform the login through the facebook
  # 
  # @params: all [GET]
  #
  # => [user_id] is the users's facebook id
  # => [email] the user's email
  # => [name] the user's facebook label name
  # => [first_name] the user's first_name
  # => [last_name] the user's last_name
  # => [access_token] the user's CURRENT token
	# => [expires_in] the access_token's expire datetime
	# => [link] the user's facebook link
	def login_facebook
    #begin
  		user = User.find_by(email: params[:email])
  		if !user.nil?
  		  puts "email exists"
  		  if logged_in?
  		    puts "already logged in"
  		    container = { "status" => "success" }
  		  elsif user.facebook_id == params[:user_id] and user.facebook_expires_in > Time.now #and user.facebook_access_token == params[:access_token]
  		    puts "logged now!"
  		    log_in user
  		    remember(user)
  		    container = { "status" => "success" }
  		  elsif user.facebook_id == params[:user_id] and user.facebook_expires_in <= Time.now and params[:expires_in].present? and params[:expires_in].present? and params[:access_token].present?
  		    puts "updating info..."
  		    # update all information
  		    user.facebook_expires_in = Time.now + params[:expires_in].to_i.second
  		    user.facebook_access_token = params[:access_token]
  		    user.facebook_name = params[:name]
  		    user.first_name = params[:first_name]
  		    user.last_name = params[:last_name]
  		    user.facebook = params[:link]
  		    # this needs to be reviewed
  		    user.password_digest = params[:access_token]
  		    if user.save
  		      puts "logged now!"
  		      log_in user
  		      remember(user)
  		      container = { "status" => "success" }
  		    else
  		      puts "update failed!"
  		      container = { "status" => "fail" }
  		    end
  		  else
  		    puts "not allowed!"
  		    container = { "status" => "not_allowed" }
  		  end
  		elsif params[:expires_in].present? and params[:access_token].present? and params[:email].present? and params[:first_name].present? and params[:last_name].present?
  		  puts "trying to register..."
  		  user = User.new
  		  user.email = params[:email]
  		  user.facebook_id = params[:user_id]
  		  user.facebook_expires_in = Time.now + params[:expires_in].to_i.second
  	    user.facebook_access_token = params[:access_token]
  	    user.facebook_name = params[:name]
  	    user.first_name = params[:first_name]
  	    user.last_name = params[:last_name]
  	    user.facebook = params[:link]
  	    user.create_adjusts
  	    # this needs to be reviewed
  	    puts "doing password thing..."
  	    user.password = params[:access_token].to_s[0, 71]
  	    
  	    puts "password thing done, trying to save..."
  	    if user.save!
  	      puts "logged now!"
  	      welcome_message user.first_name, user.email
  	      log_in user
  	      remember(user)
  	      container = { "status" => "success" }
  	    else
  	      puts "saving has failed... :("
  	      container = { "status" => "fail" }
  	    end
  		else
  		  puts "missing info..."
  		  container = { "status" => "fail" }
  		end
		#rescue
		#  puts "something wen wrong..."
		#  container = { "status" => "fail" }
		#end
		
    render :json => container.to_json
  end
end