class ApplicationController < ActionController::Base
  require 'aws-sdk'
  
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  include RepublicsHelper
  include SessionsHelper
  include WebSitesHelper
  include MessagesHelper
  
  def save_image_to_s3(uploaded_io, folder, file_name)
    filename = uploaded_io.original_filename
    extension = filename.split('.').last.downcase
    
    tmp_file = "#{Rails.root}/public/#{folder}/#{file_name}.#{extension}"
    aws_file = "#{folder}/#{file_name}.#{extension}"
    
    File.open(tmp_file, 'wb') do |f|
      f.write uploaded_io.read
    end
    
    s3 = Aws::S3::Resource.new
    obj = s3.bucket(getCountryMessage('s3_bucket')).object(aws_file)
    obj.upload_file(tmp_file, acl: 'public-read')
    
    return obj.public_url
    # return "#{getCountryMessage('s3_url')}/#{getCountryMessage('s3_bucket')}/#{aws_file}"
  end
  
  def delete_image_from_s3(s3_file)
    s3_file.slice! "#{getCountryMessage('s3_url')}/"
    s3 = Aws::S3::Resource.new
    obj = s3.bucket(getCountryMessage('s3_bucket')).object(s3_file)
    obj.delete
  end
end
