class UsersController < ApplicationController
  require 'net/smtp'
  
  def new
    user = User.new(user_params)
    user.create_adjusts
    if user.save
      log_in user
      welcome_message user.first_name, user.email
      case params[:announce_type]
        when "property"
          redirect_to register_property_path
        when "people"
          redirect_to register_people_path
        when "vacancy"
          redirect_to register_vacancy_path
      end
    else
      flash[:danger] = getTextMessage("signup_error_message")
      redirect_to root_path
    end
  end
  
  def new_republic
    if !logged_in?
      redirect_to root_path
    end
    user = current_user
    republic = Republic.new(republic_params)
    republic.user_id = user.id
    republic.active = true
    if republic.save
      if params[:pictures]
        params[:pictures].each do |image|
          picture = Picture.new
          picture.main = 0
          picture.path = "temp"
          picture.republic_id = republic.id
          if picture.save
            picture.path = save_image_to_s3(image, "pictures", "#{picture.republic_id}_#{picture.id}")
            picture.save
          else
            flash[:danger] = getTextMessage("fail_vacancy_registration")
          end
        end
      end
      message_republic_registered user.first_name, user.email
      flash[:success] = getTextMessage("success_vacancy_registration")
	    redirect_to user_home_path
	  else
      flash[:danger] = getTextMessage("fail_vacancy_registration")
      redirect_to root_path
    end
  end
  
  def home
    if !logged_in?
      redirect_to root_path
    end
    if current_user.admin
      redirect_to team_path
    end
    
    questions = Question.where("language = ?", systemLanguage)
    questions_number = questions.length
    questions_answered = 0
    user_answers = current_user.user_answers
    
    user_answers.each do |answer|
      question = answer.question
      if question.language == systemLanguage
        questions_answered += 1
      end
    end
    
    @compatibility_complete = questions_answered == questions_number
  end
  
  def team
    #if logged_in? and !current_user.admin
      @new_republics = Republic.where(latitude: nil)
      @edit_republics = Republic.all
      @messages = Message.all.order(created_at: :desc)
    #else
    #  redirect_to root_path
    #end
  end
  
  def edit
    if !logged_in?
      redirect_to root_path
    end
    if current_user.update_attributes(user_edit_params)
      flash[:success] = getTextMessage("user_alteration_success")
    else
      flash[:danger] = getTextMessage("user_alteration_fail")
    end
    redirect_to user_home_path
  end
  
  def edit_password
    if !logged_in?
      redirect_to root_path
    end
    if current_user.authenticate(params[:user][:password_old])
      if current_user.update_attributes(user_edit_password_params)
        flash[:success] = getTextMessage("password_changed_successfully")
      else
        flash[:danger] = getTextMessage("password_changed_fail")
      end
    else
      flash[:danger] = getTextMessage("invalid_old_password")
    end
    redirect_to user_home_path
  end
  
  def teste
    send_email "thiago.lc.melo@gmail.com", "um teste", signature
  end
  
  def change_password
    if !logged_in?
      user = User.find_by(email: params[:email])
      if !user.nil?
        passrecovery = PasswordRecovery.find(params[:code])
        if passrecovery.user_id == user.id and !passrecovery.used
          #passrecovery.used = true
          #passrecovery.save
          @user = user
        else
          redirect_to root_path
        end
      else
        redirect_to root_path
      end
    else
      redirect_to root_path
    end
  end
  
  def update_password
    if logged_in?
      redirect_to root_path
    end
    user = User.find(params[:user][:id])
    if !user.nil?
      passrecovery = PasswordRecovery.where(user_id: user.id).where(used: false).take
      if !passrecovery.nil?
        if user.update_attributes(user_edit_password_params)
          passrecovery.used = true
          passrecovery.save
          log_in user
          remember(user)
          redirect_to user_home_path
        else
          redirect_to root_path
        end
      else
        redirect_to root_path
      end
    else
      redirect_to root_path
    end
  end
  
  # ajax methods
  
  def recovery_password_ajax
    if logged_in?
      flash[:danger] = getTextMessage("disconected_for_security")
      log_out
      redirect_to root_url
      return
    end
    user = User.find_by(email: params[:recovery_email].downcase)
    if !user.nil?
      passrecovery = PasswordRecovery.where(user_id: user.id).where(used: false).take
      if passrecovery.nil?
        passrecovery = PasswordRecovery.new(user_id: user.id, used: false)
        passrecovery.save
      end
      password_recovery_message user.first_name, user.email, passrecovery.id
      container = { "status" => "success" }
    else
      container = { "status" => "fail" }
    end
    render :json => container.to_json
  end
  
  def delete_announce_ajax
    if !logged_in?
      redirect_to root_url
      return
    end
    republic = Republic.find(params[:id])
    if !republic.nil?
      if republic.user.id == current_user.id
        republic.active = false
        if republic.save
          container = { "status" => "success" }
        else
          container = { "status" => "fail" }
        end
      else
        flash[:danger] = getTextMessage("disconected_for_security")
        log_out
        redirect_to root_url
        return
      end
    else
      container = { "status" => "fail" }
    end
    render :json => container.to_json
  end
  
  # this should perform the signup for a user through normal way
  # using ajax request
  def new_ajax
    user = User.new(user_params)
    user.create_adjusts
    if user.save
      log_in user
      welcome_message user.first_name, user.email
      container = { "status" => "success" }
    else
      container = { "status" => "fail" }
    end
    render :json => container.to_json
  end
  
  private

    def user_params
      params.require(:user).permit(:first_name, :last_name, :email, :hide_email, :password,
        :password_confirmation, :facebook, :phone, :hide_phone)
    end
    
    def user_edit_params
      params.require(:user).permit(:first_name, :last_name, :hide_email, :email, :facebook, :phone, :hide_phone)
    end
    
    def user_edit_password_params
      params.require(:user).permit(:password, :password_confirmation)
    end
    
    def republic_params
      params.require(:republic).permit(:title, :is_people, :is_landlord, :address, :number, :shape, 
        :hide_address, :gender, :cost, :roommates, :description, :pets, :smoking, :alcohol,
        :stuff, :bathrooms, :heating, :parking, :parties, :organization, :lease, :university_id)
    end
end