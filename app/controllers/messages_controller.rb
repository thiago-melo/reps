class MessagesController < ApplicationController
  
  # it is used to see messages sent over the site
  def message_stuff
    @messages = Message.all.order(created_at: :desc)
  end
  
  # ajax methods 
  
  # it creates a record in messages table
  # and send an email to the lister
  def message_for_user_ajax
    republic = Republic.find(params[:message][:republic_id])
    if !republic.nil?
      #message = Message.where("republic_id = #{params[:message][:republic_id]} and name = '#{params[:message][:name]}' and email = '#{params[:message][:email]}' and subject = '#{params[:message][:subject]}' and created_at > '#{Time.now - 1.minute}'")
      #if message.length > 0
        message = Message.new(message_for_user_params)
        if message.update_attribute(:active, true)
          # check if the user has compatibility info save on cookies and save it
          saveUserPersonalityInfo(message.name, message.email)
          # sending email to lister
          if contact_from_website message.name, message.email, republic.user.email, republic.user.first_name, republic.title, message.subject, message.body
            message.update_attribute(:subject, "To: [#{republic.user.email}], rep_id: #{republic.id}, subject: #{message.subject}")
            container = { "message" => nil, "status" => "success"}
          else
            container = { "message" => nil, "status" => "fail"}
          end
        else
          container = { "message" => nil, "status" => "fail"}
        end
      #else
      #  container = { "message" => nil, "status" => "success"}
      #end
    else
      container = { "message" => nil, "status" => "fail"}
    end
    render :json => container.to_json
  end
  
  # it creates a record in messages table
  # and send an email to the a friend
  def message_for_friend_ajax
    republic = Republic.find(params[:message][:republic_id])
    if !republic.nil?
      message = Message.new(message_for_friend_params)
      if message.update_attribute(:active, true)
        # sending email to friend
        if contact_from_website_to_friend params[:message][:sender], message.name, message.email, message.subject, message.body
          message.update_attribute(:subject, "To: [#{message.name}], rep_id: #{republic.id}, subject: #{message.subject}")
          container = { "message" => nil, "status" => "success"}
        else
          container = { "message" => nil, "status" => "fail"}
        end
      else
        container = { "message" => nil, "status" => "fail"}
      end
    else
      container = { "message" => nil, "status" => "fail"}
    end
    render :json => container.to_json
  end
  
  def send_message_thiago_ajax
    contact_from_website params[:message][:name], params[:message][:email], "thiago.lc.melo@gmail.com", "Thiago Melo", "Message Jobs", params[:message][:subject], params[:message][:body]
    container = { "message" => nil, "status" => "success"}
    render :json => container.to_json
  end
  
  # ------------- TASK ------------------
  # It returns the register from messages table
  def message_get_ajax
    message = Message.find(params[:id])
    if !message.nil?
      container = { "message" => message, "status" => "success"}
    else
      container = { "message" => nil, "status" => "fail"}
    end
    render :json => container.to_json  
  end
  
  # It toggles to state of a message
  # the state is represented by :active
  # which can be true or false
  def message_change_ajax
    message = Message.find(params[:id])
    if message.toggle(:active).save
      container = { "status" => "success"}
    else
      container = { "status" => "fail"}
    end
    render :json => container.to_json  
  end
  
  # ------------- TASK ------------------
  # under message_stuff page, a new passaword can be set for the user
  # it will only works if the email is registered. The generated password
  # is returned itself. Take care, pretty unsafe!!
  def message_recovery_ajax
    message = Message.find(params[:id])
    if !message.nil?
      user = User.find_by(email: message.email)
      if !user.nil?
        password = rand(10).to_s + rand(10).to_s + rand(10).to_s + rand(10).to_s + rand(10).to_s + rand(10).to_s
        user.password = password
        user.password_confirmation = password
        if user.save
          container = { "status" => "success", "email" => user.email, "password" => password}
        else
          container = { "status" => "fail", "email" => user.email }
        end
      else
        container = { "status" => "not_registered", "email" => message.email}
      end
    else
      container = { "status" => "fail", "email" => message.email }
    end
    render :json => container.to_json  
  end
  
  private
    # params for create a message, take care!
    # a param named :active is need
    def message_for_user_params
      params.require(:message).permit(:republic_id, :name, :email, :subject, :body)
    end
    
    def message_for_friend_params
      params.require(:message).permit(:republic_id, :name, :email, :subject, :body)
    end
end
