module RepublicsHelper
  def rep_types
    if systemLanguage == "pt"
      if systemCountry == "br"
        return { 0 => "República", 1 => "Pensão", 2 => "Quitinete", 3 => "Alugel de Apartamento", 4 => "Aluguel de Casa" }
      elsif systemCountry == "us"
        return { 0 => "Casa", 1 => "Apartamento", 2 => "Quarto", 3 => "Studio", 4 => "Duplex" }
      end
    elsif systemLanguage == "en"
      if systemCountry == "br"
        return { 0 => "Republic", 1 => "Hostel", 2 => "Kitchenette", 3 => "Rent apartment", 4 => "Rent house" }
      elsif systemCountry == "us"
        return { 0 => "House", 1 => "Apartment", 2 => "Single room", 3=> "Studio", 4 => "Duplex" }
      end
    elsif systemLanguage == "es"
      if systemCountry == "br"
        return { 0 => "República", 1 => "Albergue", 2 => "Cocina pequeña", 3 => "Alquiler apartamentos", 4 => "Alquiler xasa" }
      elsif systemCountry == "us"
        return { 0 => "Casa", 1 => "Apartamento", 2 => "Habitación", 3=> "Studio", 4 => "Duplex" }
      end
    end
  end
  def rep_genders
    if systemLanguage == "pt"
      return { 0 => "Indiferente", 1 => "Feminina", 2 => "Masculina" }
    elsif systemLanguage == "en"
      return { 0 => "Doesnt matter", 1 => "Female", 2 => "Male" }
    elsif systemLanguage == "es"
      return { 0 => "Indiferente", 1 => "Femenino", 2 => "Macho" }
    end
  end
  def lease_options
    if systemLanguage == "pt"
      if systemCountry == "br"
        return { 0 => "Mensal avulso", 1 => "Contrato semestral", 2 => "Contrato anual", 3 => "Contrato 3 anos"  }
      elsif systemCountry == "us"
        return { 0 => "Mensal avulso", 1 => "Contrato semestral", 2 => "Contrato anual", 3 => "Sub-locação"  }
      end
    elsif systemLanguage == "en"
      if systemCountry == "br"
        return { 0 => "Month-to-month", 1 => "6 month lease", 2 => "1 year lease", 3 => "3 year lease" }
      elsif systemCountry == "us"
        return { 0 => "Month-to-month", 1 => "6 month lease", 2 => "1 year lease", 3 => "Sublease" }
      end
    elsif systemLanguage == "es"
      if systemCountry == "br"
        return { 0 => "Mes a mes", 1 => "6 meses de arrendamiento", 2 => "El año de arrendamiento 1", 3 => "El año arrendamiento 3" }
      elsif systemCountry == "us"
        return { 0 => "Mes a mes", 1 => "6 meses de arrendamiento", 2 => "1 años de arrendamiento", 3 => "subarrendamiento" }
      end
    end
  end
  def organization_options
    if systemLanguage == "pt"
      return { 0 => "Estilo militar", 1 => "Arrumadinho", 2 => "De vez em quando", 3 => "Várzea total"  }
    elsif systemLanguage == "en"
      return { 0 => "Army style", 1 => "Tidy", 2 => "So-so", 3 => "Disorganized" }
    elsif systemLanguage == "es"
      return { 0 => "Estilo militar", 1 => "Arrumadinho", 2 => "De vez en cuando", 3 => "Desordenado"  }
    end
  end
  def parties_options
    if systemLanguage == "pt"
      return { 0 => "Não tenho tempo", 1 => "Às vezes", 2 => "Frequentemente", 3 => "Baladeiro de plantão"  }
    elsif systemLanguage == "en"
      return { 0 => "I don't like parties", 1 => "Once in a while", 2 => "Once per a week", 3 => "Very often" }
    elsif systemLanguage == "es"
      return { 0 => "No me gustan las fiestas", 1 => "De vez en cuando", 2 => "Una vez por semana", 3 => "Muy a menudo" }
    end
  end
  def stuff_options
    if systemLanguage == "pt"
      return { 0 => "Careta", 1 => "Tolerante", 2 => "De vez em quando", 3 => "Stoner" }
    elsif systemLanguage == "en"
      return { 0 => "I don't tolerate", 1 => "4:20 friendly", 2 => "I smoke sometimes", 3 => "Very often" }
    elsif systemLanguage == "es"
      return { 0 => "No tolero", 1 => "04:20 amigable", 2 => "A veces fuman", 3 => "Muy a menudo" }
    end
  end
end