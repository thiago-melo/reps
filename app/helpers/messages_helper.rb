module MessagesHelper
  
  def send_email (to, subject, body, from = nil, cancel_id = nil)
    require 'net/smtp'
    from = from.nil? ? current_from["email"] : from
    
    body.gsub!(/\n/, '<br/>')
    
    message = <<-MESSAGE_END
    From: #{current_from['name']} <#{current_from['email']}>
    \r\nTo: <#{to}>
    \r\Reply-to: #{from}
    \r\nSubject: #{subject}
    \r\nMIME-Version: 1.0
    \r\nContent-type: text/html
    \r\n
    #{body}
    MESSAGE_END
    #Using Block
    smtp = Net::SMTP.new('smtp.gmail.com', 587 )
    smtp.enable_starttls
    smtp.start('gmail.com', current_from["email"], current_from["password"], :login) do |smtp_do|
            smtp_do.send_message message, current_from["email"], to
    end
  end
  
  def current_from
    if systemCountry == "br"
      return { "email" => "unicoruja@gmail.com", "password" => "un1c0ruj4", "name" => "UniCoruja" }
    end
    return { "email" => "uniowl.team@gmail.com", "password" => "un10wlus4", "name" => "UniOwl" }
  end
  
  def password_recovery_message (first_name, email, password_change_code)
    message = getTextMessage("message_person_title") + " #{first_name},"
    message += "<br><br>"
    message += getTextMessage("password_recovery_message_body")
    message += "<br><br>"
    message += "http://" + getTextMessage("domain") + "/change_password?code=#{password_change_code}&email=#{email}"
    message += "<br><br>"
    message += signature
    send_email email, getTextMessage("password_recovery_message_subject"), message
  end
  
  def signature
    signature_message = "<br><br>"
    signature_message = getTextMessage("best_regards") + ","
    signature_message += "<br><br>"
    signature_message += "<b>" + getTextMessage("reps_team") + "</b>"
    signature_message += "<br><br>"
    signature_message += "<b>" + getTextMessage("follow_us") + ":</b>"
    signature_message += "<br><br>"
    signature_message += "<a href='https://www.facebook.com/" + getTextMessage("facebook_page") + "'><img src='http://" + getTextMessage("domain") + "/assets/img/facebook.png'></a>"
    signature_message += "<a href='https://twitter.com/" + getTextMessage("twitter_page") + "'><img src='http://" + getTextMessage("domain") + "/assets/img/twitter.png'></a>"
    signature_message += "<a href='https://plus.google.com/" + getTextMessage("googlep_page") + "'><img src='http://" + getTextMessage("domain") + "/assets/img/googlep.png'></a>"
    return signature_message
  end
  
  def contact_from_website (name, from, to, first_name, title, subject, body)
    #begin
      text_message = getTextMessage("message_person_title") + " #{first_name},"
      text_message += "<br><br>"
      text_message += getTextMessage("message_from_website_subject") + title
      text_message += "<br><br>"
      text_message += "-----------------------------------------------------------"
      text_message += "<br><br>"
      text_message += "<b>" + getTextMessage("name") + "</b>: #{name}"
      text_message += "<br>"
      text_message += "<b>" + getTextMessage("email") + "</b>: #{from}"
      text_message += "<br>"
      text_message += "<b>" + getTextMessage("subject") + "</b>: #{subject}"
      text_message += "<br><br>"
      text_message += body
      text_message += "<br><br>"
      text_message += "-----------------------------------------------------------"
      text_message += "<br><br>"
      text_message += getTextMessage("message_from_website_go_to_home")
      text_message += "<br><br>"
      text_message += "http://" + getTextMessage("domain")
      text_message += "<br><br>"
      text_message += signature
      send_email to, getTextMessage("message_from_website_subject_email"), text_message, from
      return true
    #rescue
    #  return false
    #ensure 
      # nothing
    #end
  end
  
  # this method send a message to a user's friend
  # it is a listing being suggested
  def contact_from_website_to_friend (friend, name, to, subject, body)
    #begin
      text_message = getTextMessage("message_person_title") + " #{name},"
      text_message += "<br><br>"
      text_message += "-----------------------------------------------------------"
      text_message += "<br><br>"
      text_message += body
      text_message += "<br><br>"
      text_message += friend
      text_message += "<br><br>"
      text_message += "-----------------------------------------------------------"
      text_message += "<br><br>"
      text_message += getTextMessage("message_from_website_go_to_home")
      text_message += "<br><br>"
      text_message += "http://" + getTextMessage("domain")
      text_message += "<br><br>"
      text_message += signature
      send_email to, subject, text_message
      return true
    #rescue
    #  return false
    #ensure 
      # nothing
    #end
  end
  
  def welcome_message (first_name, email)
    text_message = getTextMessage("message_person_title") + " #{first_name},"
    text_message += "<br><br>"
    text_message += getTextMessage("message_from_website_welcome_body")
    text_message += "<br><br>"
    text_message += "http://" + getTextMessage("domain")
    text_message += "<br><br>"
    text_message += getTextMessage("message_from_website_welcome_social")
    text_message += "<br><br>"
    text_message += signature
    send_email email, getTextMessage("message_from_website_welcome_subject"), text_message
  end
  
  def message_republic_registered (first_name, email)
    text_message = getTextMessage("message_person_title") + " #{first_name},"
    text_message += "<br><br>"
    text_message += getTextMessage("message_from_website_registering_body")
    text_message += "<br><br>"
    text_message += getTextMessage("message_from_website_welcome_social")
    text_message += "<br><br>"
    text_message += signature
    send_email email, getTextMessage("message_from_website_registering_subject"), text_message
  end
  
  # this method saves the user compatibility's answers
  # it should be applayed whenever the user provides his/her email
  # so, it will check if there is some cookies saved, if so, then save
  # 
  # @params: [name] the user name
  #          [email] the user email
  def saveUserPersonalityInfo(name, email)
    if cookies[:compatibility_answers].present?
      cookie_answers = ActiveSupport::JSON.decode(cookies[:compatibility_answers])
      i = 0
      questions = Array.new
      cookie_answers.each_with_index do |question, index|
        if !question.nil?
          questions[i] = "#{index.to_s}=#{question.join(',')}"
          i += 1
        end
      end
      personality = Personality.new
      personality.name = name
      personality.email = email
      personality.answers = questions.join('|')
      personality.save
    end
  end
end
