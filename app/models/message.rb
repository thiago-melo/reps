class Message < ActiveRecord::Base
  validates :name, presence: true
  validates :email, presence: true
  validates :subject, presence: true
  validates :body, presence: true
  validates :active, :inclusion => {:in => [true, false]}
  
  belongs_to :republic
end
