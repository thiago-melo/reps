class Newsletter < ActiveRecord::Base
  validates :batch_number, presence: true
  validates :newsletter_email_id, presence: true
  validates :view, presence: true
  
  belongs_to :newsletter_email
end
