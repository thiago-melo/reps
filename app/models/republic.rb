class Republic < ActiveRecord::Base
  
  validates :user_id, presence: true
  validates :shape, presence: true
  validates :cost, presence: true
  #validates :gender, presence: true
  validates :title, presence: true
  validates :active, :inclusion => {:in => [true, false]}
  
  belongs_to :user
  belongs_to :university
  
  has_many :pictures, dependent: :destroy
  has_many :messages, dependent: :destroy
  
  # this method receive the user compatibility answers and return a score
  # which is {0, 1, 2, 3, 4, 5} this score is based on the republic.user
  # answers.
  #
  # 0 means the users hasn't filled the questions
  # 5 means high compatibility
  # 1 means low compatibility
  #
  # @params: [answers] a array with the answers from the person searching
  #          [university_id]
  #          [language]
  #
  # @author: Thiago Melo
  # @version: 2015-03-29
  def userCompatibility(answers, university_id, language)
    user = self.user
    user_answers = user.user_answers#.where("university_id = ?  and language = ?", university_id, language)
    
    user_answers = (UserAnswer.joins(:question).where(questions: { :language => language, :university_id => university_id })).where(:user_id => user.id)
    
    if user_answers.length == 0 or answers.length == 0
      return 0
    end
    
    score = 0
    
    answers.each_with_index do |answer, index|
      if !answer.nil?
        answer.each_with_index do |sub_answer, sub_index|
          user_answers.each do |user_answer|
            if user_answer.question_id == index and user_answer.answer_id.to_s == sub_answer
              score += 1
            end
          end
          
        end
      end
    end
    
    return [((score.to_f / ([answers.length, user_answers.length].min).to_f) * 5).ceil, 5].min
    
    #return rand(0..5)
  end
end