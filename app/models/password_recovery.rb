class PasswordRecovery < ActiveRecord::Base
  validates :user_id, presence: true
  validates :used, :inclusion => {:in => [true, false]}
  belongs_to :user
end
