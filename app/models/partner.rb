class Partner < ActiveRecord::Base
  validates :name, presence: true
  validates :active, :inclusion => {:in => [true, false]}
  validates :latitude, presence: true
  validates :longitude, presence: true
  #validates :small_logo, presence: true
  #validates :logo, presence: true
end
