class Question < ActiveRecord::Base
  
  validates :university_id, presence: true
  validates :question, presence: true
  validates :active, :inclusion => {:in => [true, false]}
  
  belongs_to :university
  
  has_many :answers, dependent: :destroy
  has_many :user_answers, dependent: :destroy
  has_many :users, through: :user_answers
end
