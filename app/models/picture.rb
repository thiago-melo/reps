class Picture < ActiveRecord::Base
  validates :republic_id, presence: true
  validates :main, :inclusion => {:in => [true, false]}
  validates :path, presence: true
  
  belongs_to :republic, counter_cache: true
end
