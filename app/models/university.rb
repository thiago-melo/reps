class University < ActiveRecord::Base
  
  validates :name, presence: true
  validates :acronym, presence: true
  validates :latitude, presence: true
  validates :longitude, presence: true
  validates :zoom, presence: true
  #validates :logo, presence: true
  validates :active, :inclusion => {:in => [true, false]}
  
  has_many :republics, dependent: :destroy
  
  has_many :questions
  has_many :answers, through: :questions
end
