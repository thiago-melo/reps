class User < ActiveRecord::Base
  attr_accessor :remember_token
  
  before_save { self.email = email.downcase }
  validates :first_name, presence: true, length: { maximum: 25 }
  validates :last_name, presence: true, length: { maximum: 25 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
  validates :active, :inclusion => {:in => [true, false]}
  validates :premium, :inclusion => {:in => [true, false]}
  validates :admin, :inclusion => {:in => [true, false]}
  has_secure_password
  validates :password, presence: true, length: {minimum: 6}, on: :create
  validates :password, length: {minimum: 6}, on: :update, allow_blank: true
  
  has_many :user_answers, dependent: :destroy
  has_many :answers, through: :user_answers
  has_many :questions, through: :user_answers
  
  has_many :republics, dependent: :destroy
  has_many :password_recoveries, dependent: :destroy
  
  def create_adjusts
    self.active = 1
    self.premium = 0
    self.admin = 0
    if self.last_name.include? "'"
      self.username = self.first_name.downcase + "_" + self.last_name.downcase.gsub("'","")
    else
      self.username = self.first_name.downcase + "_" + self.last_name.downcase
    end
    if self.username.include? " "
      self.username = self.username.gsub(" ","_")
    end
    if self.username.include? "-"
      self.username = self.username.gsub("-","_")
    end
    if self.username.include? "."
      self.username = self.username.gsub(".","")
    end
    id = 0
    username = self.username
    while User.where("username = '#{self.username}'").length > 0 do
      self.username = "#{username}_#{id}"        
      id += 1
    end
  end
  
  # Returns the hash digest of the given string.
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  # Returns a random token.
  def User.new_token
    SecureRandom.urlsafe_base64
  end

  # Remembers a user in the database for use in persistent sessions.
  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end
  
  # Returns true if the given token matches the digest.
  def authenticated?(remember_token)
    BCrypt::Password.new(remember_digest).is_password?(remember_token)
  end
  
  # Forgets a user.
  def forget
    update_attribute(:remember_digest, nil)
  end
  
  def clean_for_ajax
    return self.slice(:id, :first_name, :last_name, :hide_email, :phone, :hide_phone, :email, :facebook)
  end
end
