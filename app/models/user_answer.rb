class UserAnswer < ActiveRecord::Base
  
  validates :question_id, presence: true
  validates :answer_id, presence: true
  validates :user_id, presence: true
  
  belongs_to :question
  belongs_to :answer
  belongs_to :user
  
end
