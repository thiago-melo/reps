class Answer < ActiveRecord::Base
  
  validates :question_id, presence: true
  validates :answer, presence: true
  validates :active, :inclusion => {:in => [true, false]}
  
  belongs_to :question
  
  has_many :user_answers, dependent: :destroy
  has_many :users, through: :user_answers
end
