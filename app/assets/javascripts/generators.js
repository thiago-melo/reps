/**
 * this function creates a whole complete modal with size normal
 * this modal id is received as a param
 * 
 * the modal's header is going to has id: [modal_id]_title
 * the modal's body is going to has id: [modal_id]_body
 * the modal's footer is going to has id: [modal_id]_footer
 * 
 * @params: [modal_id] the id for the generated modal
 * 
 * @author: Thiago Melo
 * @version: 2015-03-26
 * 
 */
var modalNormalComplete = function(modal_id) {
  return modalComplete(modal_id, 'normal');
};

/**
 * this function creates a whole complete modal with size large
 * this modal id is received as a param
 * 
 * the modal's header is going to has id: [modal_id]_title
 * the modal's body is going to has id: [modal_id]_body
 * the modal's footer is going to has id: [modal_id]_footer
 * 
 * @params: [modal_id] the id for the generated modal
 * 
 * @author: Thiago Melo
 * @version: 2015-03-26
 * 
 */
var modalLargeComplete = function(modal_id) {
  return modalComplete(modal_id, 'large');
};

/**
 * this function creates a whole complete modal with size small
 * this modal id is received as a param
 * 
 * the modal's header is going to has id: [modal_id]_title
 * the modal's body is going to has id: [modal_id]_body
 * the modal's footer is going to has id: [modal_id]_footer
 * 
 * @params: [modal_id] the id for the generated modal
 * 
 * @author: Thiago Melo
 * @version: 2015-03-26
 * 
 */
var modalSmallComplete = function(modal_id) {
  return modalComplete(modal_id, 'small');
};

/**
 * this function creates a whole complete modal with size specified
 * this modal id is received as a param
 * 
 * the modal's header is going to has id: [modal_id]_title
 * the modal's body is going to has id: [modal_id]_body
 * the modal's footer is going to has id: [modal_id]_footer
 * 
 * @params: [modal_id] the id for the generated modal
 *          [size] the size maybe {'normal', 'large', 'small'}
 * 
 * @author: Thiago Melo
 * @version: 2015-03-26
 * 
 */
var modalComplete = function(modal_id, size) {
  switch (size) {
    case 'normal':
      size = '';
      break;
    case 'small':
      size = ' modal-sm';
      break;
    case 'large':
      size = ' modal-lg';
      break;
    default:
      size = '';
  }
  var modal = $('<div/>').addClass('modal fade').prop('id', modal_id).prop('tabindex', '-1').prop('role', 'dialog').prop('aria-labelledby', modal_id + 'ModalLabel')
    .prop('aria-hidden', 'true').append(
    $('<div/>').addClass('modal-dialog' + size).append(
      $('<div/>').addClass('modal-content').append(
        $('<div/>').addClass('modal-header').append(
          $('<button/>').addClass('close').prop('type', 'button').prop('aria-label', 'Close').append(
            $('<span/>').prop('aria-hidden', 'true').append('&times;')
          ).click(function() {
            $('#' + modal_id).modal('hide');
          })
        ).append(' ').append($('<h4/>').addClass('modal-title').prop('id', modal_id + '_title'))
      ).append($('<div/>').addClass('modal-body').prop('id', modal_id + '_body')).append($('<div/>').addClass('modal-footer').prop('id', modal_id + '_footer'))
    )
  ).appendTo($('#modal_temp').empty());
  return modal;
};

/*----------------------------------------------------------------------*/
/**
 * this class create a small wrapper around the modal
 * making available methods to control it
 * 
 * @params: [modal_id] the id for the generated modal
 *          [size] the size maybe {'normal', 'large', 'small'}
 * 
 * @author: Thiago Melo
 * @version: 2015-03-27
 * 
 */
var ModalWrapper = function(modal_id, size) {
  this.modal = modalComplete(modal_id, size);
  this.modal_id = modal_id;
  this.size = size;
  
  this.setTitle = function(title) {
    $('#' + this.modal_id + '_title').empty().append(title);
  };
  
  this.setBody = function(body) {
    $('#' + this.modal_id + '_body').empty().append(body);
  };
  
  this.setFooter = function(footer) {
    $('#' + this.modal_id + '_footer').empty().append(footer);
  };
  
  this.show = function() {
    $('#' + this.modal_id).modal('show');
  };
  
  this.hide = function() {
    $('#' + this.modal_id).modal('hide');
  };
  
  this.restart = function() {
    this.modal.remove();
    this.modal = modalComplete(this.modal_id, this.size);
  };
};

/*----------------------------------------------------------------------*/




/*----------------------------------------------------------------------*/
/*---------------------- Bootstrap buildings ---------------------------*/

var bootstrapRow = function(cols) {
  return $('<div/>').addClass('row').append(cols);
};

var bootstrapCol = function(lg, md, sm, xs, oslg, osmd, ossm, osxs) {
  return $('<div/>').addClass('col-lg-' + lg).addClass('col-md-' + md).addClass('col-sm-' + sm).addClass('col-xs-' + xs)
    .addClass('col-lg-offset-' + oslg).addClass('col-md-offset-' + osmd).addClass('col-sm-offset-' + ossm).addClass('col-xs-offset-' + osxs);
};

var bootstrapFastCol = function(md, sm, osmd, ossm) {
  return bootstrapCol(md, md, sm, 12, osmd, osmd, ossm, 0);
};

var icon = function(name) {
  return $('<i/>').addClass('fa fa-' + name);
};

var iconSpinning = function(name) {
  return icon(name).addClass('fa-spin');
};


/*----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*/
/*--------------------- Button Actions ---------------------------------*/

var buttons_backup = [];

var processingButton = function(button_id) {
  var alreadyHas = false;
  var btn_changing = $('#' + button_id);
	$.each(buttons_backup, function(button_index, button) {
		if (button[0] == button_id) {
			alreadyHas = true;
		}
	});
	if (!alreadyHas) {
		buttons_backup.push([button_id, btn_changing.attr('class'), btn_changing.text()]);
	}
	btn_changing.empty().removeClass().addClass('btn btn-info').append(iconSpinning('cog')).append(' ' + getTextMessage['processing']);
};

var remakeButton = function(button_id) {
  $.each(buttons_backup, function(button_index, button) {
		if (button[0] == button_id) {
			$('#' + button_id).empty().removeClass().addClass(button[1]).append(button[2]);	
		}
	});
};

var buttonLoading = function(button_id, callback) {
  processingButton(button_id);
  setTimeout(function(){ callback(); }, 200);
};

/*----------------------------------------------------------------------*/