Rails.application.routes.draw do
  
  # messages controller routes
  match 'message_stuff', :to => 'messages#message_stuff', via: 'get'
  match 'message_get', :to => 'messages#message_get_ajax', via: 'get'
  match 'message_change', :to => 'messages#message_change_ajax', via: 'get'
  match 'message_recovery', :to => 'messages#message_recovery_ajax', via: 'get'

  # republics controller routes
  get 'republics/list'
  match 'repuplic_information', :to => 'republics#repuplic_information_ajax', via: 'get'
  match 'update_republic', :to => 'republics#update_republic_ajax', via: 'get'
  match 'upload_image', :to => 'republics#upload_image_ajax', via: 'post'
  match 'delete_image', :to => 'republics#delete_image_ajax', via: 'get'
  match 'filter_republics', :to => 'republics#filter_republics_ajax', via: 'get'
  match 'increment_phone_views',  :to => 'republics#increment_phone_views_ajax', via: 'get'
  match 'phone_views_number',  :to => 'republics#phone_views_number', via: 'get'

  # users controller route
  match 'signup', :to => 'users#new', via: 'post'
  match 'signup_user', :to => 'users#new_ajax', via: 'post'
  match 'new_republic', :to => 'users#new_republic', via: 'post'
  match 'user_home', :to => 'users#home', via: 'get'
  match 'user_edit', :to => 'users#edit', via: 'patch'
  match 'user_edit_password', :to => 'users#edit_password', via: 'patch'
  match 'user_change_password', :to => 'users#update_password', via: 'patch'
  match 'recovery_password', :to => 'users#recovery_password_ajax', via: 'get'
  match 'delete_announce', :to => 'users#delete_announce_ajax', via: 'get'
  match 'teste', :to => 'users#teste', via: 'get'
  match 'team', :to => 'users#team', via: 'get'
  match 'change_password',  :to => 'users#change_password', via: 'get'

  # web_sites controller routes
  root :to => 'web_sites#index'
  match 'index', :to => 'web_sites#index', via: 'get'
  match 'register_people', :to => 'web_sites#register_people', via: 'get'
  match 'register_vacancy', :to => 'web_sites#register_vacancy', via: 'get'
  match 'register_property', :to => 'web_sites#register_property', via: 'get'
  match 'search_people', :to => 'web_sites#search_people', via: 'get'
  match 'search_filter', :to => 'web_sites#search_filter', via: 'get'
  match 'search', :to => 'web_sites#search', via: 'get'
  match 'send_message', :to => 'web_sites#new_message', via: 'get'
  match 'send_message_thiago', :to => 'messages#send_message_thiago_ajax', via: 'get'
  match 'send_message_for_user', :to => 'messages#message_for_user_ajax', via: 'get'
  match 'send_message_for_friend', :to => 'messages#message_for_friend_ajax', via: 'get'
  match 'save_maps',  :to => 'web_sites#save_maps_ajax', via: 'get'
  match 'activate',  :to => 'web_sites#activate_ajax', via: 'get'
  match 'update_coordinates',  :to => 'web_sites#update_coordinates_ajax', via: 'get'
  match 'listing_preview',  :to => 'web_sites#listing_preview', via: 'get'
  match 'change_lang',  :to => 'web_sites#change_lang', via: 'get'
  match 'enable',  :to => 'web_sites#enable', via: 'get'
  match 'enable_delete',  :to => 'web_sites#enable_delete_ajax', via: 'get'
  match 'maps',  :to => 'web_sites#maps', via: 'get'
  match 'email_user',  :to => 'web_sites#email_user', via: 'get'
  
  match 'partners_help',  :to => 'web_sites#partners_help', via: 'get'
  match 'new_partner', :to => 'web_sites#new_partner_ajax', via: 'post'
  match 'partner_info',  :to => 'web_sites#partner_info_ajax', via: 'get'
  match 'update_partner',  :to => 'web_sites#update_partner_ajax', via: 'post'
  match 'partners',  :to => 'web_sites#partners', via: 'get'
  
  match 'university_help',  :to => 'web_sites#university_help', via: 'get'
  match 'new_university', :to => 'web_sites#new_university_ajax', via: 'post'
  match 'university_info',  :to => 'web_sites#university_info_ajax', via: 'get'
  match 'update_university',  :to => 'web_sites#update_university_ajax', via: 'post'
  match 'set_system_university',  :to => 'web_sites#set_system_university_ajax', via: 'get'
  
  match 'questions_help',  :to => 'web_sites#questions_help', via: 'get'
  match 'university_questions',  :to => 'web_sites#university_questions_ajax', via: 'get'
  match 'new_question', :to => 'web_sites#new_question_ajax', via: 'post'
  match 'question_answers', :to => 'web_sites#question_answers_ajax', via: 'get'
  match 'update_question', :to => 'web_sites#update_question_ajax', via: 'post'
  match 'update_answer', :to => 'web_sites#update_answer_ajax', via: 'get'
  
  match 'answer_compatibility_process', :to => 'web_sites#answer_compatibility_process_ajax', via: 'get'
  match 'quick_answer', :to => 'web_sites#quick_answer_ajax', via: 'get'
  match 'reset_questions', :to => 'web_sites#reset_questions_ajax', via: 'get'
  
  # thiago_melo_resume
  match 'thiago_melo_resume_pt',  :to => 'web_sites#thiago_melo_resume_pt', via: 'get'
  match 'thiago_melo_resume_en',  :to => 'web_sites#thiago_melo_resume_en', via: 'get'
  
  
  
  #match 'mobile',  :to => 'web_sites#mobile', via: 'get'
  #match 'message_stuff',  :to => 'web_sites#message_stuff', via: 'get'
  # sessions controller routes
  get 'sessions/new'
  get    'login'   => 'sessions#new'
  match 'login_facebook',  :to => 'sessions#login_facebook', via: 'post'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'
  
  #resources
  resources :users
  
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
