require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
    @user = User.new(
      first_name: "Fulano",
      last_name: "De Tal",
      email: "fulano@domain.com",
      phone: "Tel: (11) 9999-9999, Cel: (11) 99999-9999",
      facebook: "https://www.facebook.com/fulano.de.tal",
      username: "fulano_de_tal",
      password: "foobar",
      password_confirmation: "foobar",
      active: true,
      premium: false
    )
  end

  test "should be valid" do
    assert @user.valid?
  end
  
  test "email should be present" do
    @user.email = ""
    assert_not @user.valid?
  end
  
  test "first_name should be present" do
    @user.first_name = ""
    assert_not @user.valid?
  end
  
  test "last_name should be present" do
    @user.last_name = ""
    assert_not @user.valid?
  end
  
  test "active should be present" do
    @user.active = ""
    assert_not @user.valid?
  end
  
  test "premium should be present" do
    @user.premium = ""
    assert_not @user.valid?
  end
  
  test "first_name should not be too long" do
    @user.first_name = "a" * 26
    assert_not @user.valid?
  end
  
  test "last_name should not be too long" do
    @user.last_name = "a" * 26
    assert_not @user.valid?
  end

  test "email should not be too long" do
    @user.email = "a" * 256
    assert_not @user.valid?
  end
  
  test "email validation should accept valid addresses" do
    valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @user.email = valid_address
      assert @user.valid?, "#{valid_address.inspect} should be valid"
    end
  end
  
  test "email validation should reject invalid addresses" do
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com]
    invalid_addresses.each do |invalid_address|
      @user.email = invalid_address
      assert_not @user.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end
  
  test "email addresses should be unique" do
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.upcase
    @user.save
    assert_not duplicate_user.valid?
  end
  
  test "password should have a minimum length" do
    @user.password = @user.password_confirmation = "a" * 5
    assert_not @user.valid?
  end
end