class AddNumberToRepublics < ActiveRecord::Migration
  def change
    add_column :republics, :number, :integer, default: 0
  end
end
