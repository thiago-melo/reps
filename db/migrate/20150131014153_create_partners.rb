class CreatePartners < ActiveRecord::Migration
  def change
    create_table :partners do |t|
      t.string :name
      t.string :address
      t.string :phone
      t.string :email
      t.string :website
      t.string :facebook
      t.string :description
      t.boolean :active
      t.string :logo
      t.string :picture

      t.timestamps null: false
    end
  end
end
