class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :phone
      t.string :facebook
      t.string :username
      t.boolean :active
      t.boolean :premium

      t.timestamps null: false
    end
  end
end
