class CreateNewsletters < ActiveRecord::Migration
  def change
    create_table :newsletters do |t|
      t.integer :batch_number
      t.integer :newsletter_email_id
      t.boolean :view, default: false

      t.timestamps null: false
    end
  end
end
