class AddRepublicIdToMessages < ActiveRecord::Migration
  def change
    add_column :messages, :republic_id, :integer, default: 0
  end
end
