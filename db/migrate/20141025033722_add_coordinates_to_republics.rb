class AddCoordinatesToRepublics < ActiveRecord::Migration
  def change
    add_column :republics, :latitude, :float
    add_column :republics, :longitude, :float
  end
end
