class AddIsPeopleToRepublics < ActiveRecord::Migration
  def change
    add_column :republics, :is_people, :boolean, default: false
  end
end
