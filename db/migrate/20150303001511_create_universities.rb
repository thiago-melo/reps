class CreateUniversities < ActiveRecord::Migration
  def change
    create_table :universities do |t|
      t.string :name
      t.string :acronym
      t.float :latitude
      t.float :longitude
      t.integer :zoom
      t.string :logo

      t.timestamps null: false
    end
  end
end
