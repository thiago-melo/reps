class AddAdvancedOptionsToRepublics < ActiveRecord::Migration
  def change
    add_column :republics, :is_landlord, :boolean, default: false
    add_column :republics, :pets, :boolean, default: false
    add_column :republics, :smoking, :boolean, default: false
    add_column :republics, :alcohol, :boolean, default: true
    add_column :republics, :stuff, :integer, default: 0
    add_column :republics, :bathrooms, :integer, default: 1
    add_column :republics, :heating, :boolean, default: false
    add_column :republics, :parking, :boolean, default: false
    add_column :republics, :parties, :integer, default: 0
    add_column :republics, :organization, :integer, default: 0
    add_column :republics, :leasing, :integer, default: 0
  end
end
