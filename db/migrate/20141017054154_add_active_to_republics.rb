class AddActiveToRepublics < ActiveRecord::Migration
  def change
    add_column :republics, :active, :boolean, after: :roommates
  end
end
