class ChangeBathroomsToFloatToRepublics < ActiveRecord::Migration
  def change
    change_column :republics, :bathrooms, :float
  end
end
