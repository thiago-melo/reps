class AddMaxAnswersToQuestions < ActiveRecord::Migration
  def change
    add_column :questions, :max_answers, :integer, default: 1
  end
end
