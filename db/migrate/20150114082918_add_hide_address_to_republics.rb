class AddHideAddressToRepublics < ActiveRecord::Migration
  def change
    add_column :republics, :hide_address, :boolean, default: false
  end
end
