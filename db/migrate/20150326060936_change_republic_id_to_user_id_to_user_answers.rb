class ChangeRepublicIdToUserIdToUserAnswers < ActiveRecord::Migration
  def change
    rename_column :user_answers, :republic_id, :user_id
  end
end
