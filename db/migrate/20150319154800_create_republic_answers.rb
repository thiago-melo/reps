class CreateRepublicAnswers < ActiveRecord::Migration
  def change
    create_table :republic_answers do |t|
      t.integer :republic_id
      t.integer :question_id
      t.integer :answer_id

      t.timestamps null: false
    end
  end
end
