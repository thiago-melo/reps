class ChangeLeasingToLeaseToRepublics < ActiveRecord::Migration
  def change
    rename_column :republics, :leasing, :lease
  end
end
