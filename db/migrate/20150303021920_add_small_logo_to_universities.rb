class AddSmallLogoToUniversities < ActiveRecord::Migration
  def change
    add_column :universities, :small_logo, :string
  end
end
