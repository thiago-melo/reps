class AddPicturesCountToRepublics < ActiveRecord::Migration
  def change
    add_column :republics, :pictures_count, :integer, :default => 0
    
    Republic.reset_column_information
    Republic.all.each do |r|
      Republic.update_counters r.id, :pictures_count => r.pictures.length
    end
  end
end
