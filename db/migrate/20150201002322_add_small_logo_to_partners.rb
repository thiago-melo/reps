class AddSmallLogoToPartners < ActiveRecord::Migration
  def change
    add_column :partners, :small_logo, :string
  end
end
