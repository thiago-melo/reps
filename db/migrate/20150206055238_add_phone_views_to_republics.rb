class AddPhoneViewsToRepublics < ActiveRecord::Migration
  def change
    add_column :republics, :phone_views, :integer, default: 0
  end
end
