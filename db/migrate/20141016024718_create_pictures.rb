class CreatePictures < ActiveRecord::Migration
  def change
    create_table :pictures do |t|
      t.integer :republic_id
      t.boolean :main
      t.string :path

      t.timestamps null: false
    end
  end
end
