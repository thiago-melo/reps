class AddUniversityIdToRepublics < ActiveRecord::Migration
  def change
    add_column :republics, :university_id, :integer, default: 1
  end
end
