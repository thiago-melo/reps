class ChangeRepublicAnswerToUserAnswer < ActiveRecord::Migration
  def change
    rename_table :republic_answers, :user_answers
  end
end
