class CreateNewsletterEmails < ActiveRecord::Migration
  def change
    create_table :newsletter_emails do |t|
      t.string :email
      t.boolean :active, default: false

      t.timestamps null: false
    end
  end
end
