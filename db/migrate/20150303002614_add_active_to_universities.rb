class AddActiveToUniversities < ActiveRecord::Migration
  def change
    add_column :universities, :active, :boolean
  end
end
