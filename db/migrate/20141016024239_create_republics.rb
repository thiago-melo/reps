class CreateRepublics < ActiveRecord::Migration
  def change
    create_table :republics do |t|
      t.integer :user_id
      t.integer :shape
      t.string :address
      t.string :title
      t.integer :gender
      t.integer :cost
      t.string :description
      t.integer :host_duration
      t.integer :roommates

      t.timestamps null: false
    end
  end
end
