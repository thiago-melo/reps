class AddHidePhoneToUsers < ActiveRecord::Migration
  def change
    add_column :users, :hide_phone, :boolean, default: false
  end
end
