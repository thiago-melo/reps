class AddFacebookFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :facebook_access_token, :string, default: nil
    add_column :users, :facebook_id, :string, default: nil
    add_column :users, :facebook_name, :string, default: nil
    add_column :users, :facebook_expires_in, :datetime, default: nil
  end
end
