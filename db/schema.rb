# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150330050137) do

  create_table "answers", force: true do |t|
    t.string   "answer"
    t.boolean  "active"
    t.integer  "question_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "messages", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "subject"
    t.string   "body"
    t.boolean  "active"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "republic_id", default: 0
  end

  create_table "newsletter_emails", force: true do |t|
    t.string   "email"
    t.boolean  "active",     default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "newsletters", force: true do |t|
    t.integer  "batch_number"
    t.integer  "newsletter_email_id"
    t.boolean  "view",                default: false
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  create_table "partners", force: true do |t|
    t.string   "name"
    t.string   "address"
    t.string   "phone"
    t.string   "email"
    t.string   "website"
    t.string   "facebook"
    t.string   "description"
    t.boolean  "active"
    t.string   "logo"
    t.string   "picture"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.float    "latitude"
    t.float    "longitude"
    t.string   "small_logo"
  end

  create_table "password_recoveries", force: true do |t|
    t.integer  "user_id"
    t.boolean  "used"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "personalities", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "answers"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pictures", force: true do |t|
    t.integer  "republic_id"
    t.boolean  "main"
    t.string   "path"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "questions", force: true do |t|
    t.string   "question"
    t.boolean  "active"
    t.integer  "university_id"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "max_answers",   default: 1
    t.string   "language"
  end

  create_table "republics", force: true do |t|
    t.integer  "user_id"
    t.integer  "shape"
    t.string   "address"
    t.string   "title"
    t.integer  "gender"
    t.integer  "cost"
    t.string   "description"
    t.integer  "host_duration"
    t.integer  "roommates"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.boolean  "active"
    t.float    "latitude"
    t.float    "longitude"
    t.boolean  "is_people",      default: false
    t.boolean  "is_landlord",    default: false
    t.boolean  "pets",           default: false
    t.boolean  "smoking",        default: false
    t.boolean  "alcohol",        default: true
    t.integer  "stuff",          default: 0
    t.float    "bathrooms",      default: 1.0
    t.boolean  "heating",        default: false
    t.boolean  "parking",        default: false
    t.integer  "parties",        default: 0
    t.integer  "organization",   default: 0
    t.integer  "lease",          default: 0
    t.integer  "number",         default: 0
    t.boolean  "hide_address",   default: false
    t.integer  "phone_views",    default: 0
    t.integer  "university_id",  default: 1
    t.integer  "pictures_count", default: 0
  end

  create_table "universities", force: true do |t|
    t.string   "name"
    t.string   "acronym"
    t.float    "latitude"
    t.float    "longitude"
    t.integer  "zoom"
    t.string   "logo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean  "active"
    t.string   "small_logo"
  end

  create_table "user_answers", force: true do |t|
    t.integer  "user_id"
    t.integer  "question_id"
    t.integer  "answer_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "users", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "phone"
    t.string   "facebook"
    t.string   "username"
    t.boolean  "active"
    t.boolean  "premium"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.string   "password_digest"
    t.string   "remember_digest"
    t.boolean  "hide_email",            default: false
    t.boolean  "admin",                 default: false
    t.boolean  "hide_phone",            default: false
    t.string   "facebook_access_token"
    t.string   "facebook_id"
    t.string   "facebook_name"
    t.datetime "facebook_expires_in"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true

end
